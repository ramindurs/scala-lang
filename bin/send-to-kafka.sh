#!/usr/bin/env bash

if [ $# -ne 3 ] ; then echo "Usage $0 <topic> <key> <value>"; exit 1 ;fi

echo "$2:$3" | \
    /usr/local/kafka/bin/kafka-console-producer \
    --broker-list localhost:9093 \
    --topic "$1" \
    --property "parse.key=true" \
    --property "key.separator=:" \
    --property "log.cleanup.policy=compact"