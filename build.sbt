name := "scala-lang"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies ++= {
  val AkkaVersion = "2.5.19"
  val AkkaKafkaStreamVersion = "0.22"
  val AkkaHttpVersion = "10.1.7"
  val Json4sVersion = "3.5.2"
  val LogbackClassicVersion = "1.2.3"
  val lazyLoggingVersion = "3.9.2"
  val ScalaTestVersion = "3.0.5"
  val circeVersion = "0.10.0"
  val sparkVersion = "2.4.1"
  val CatsVersion = "1.6.0"
  val CatsEffectVersion = "1.3.0"
  val ScalaCheckVersion = "1.13.4"
  val doobieVersion = "0.6.0"
  val slickVersion = "3.3.0"
  val h2Version = "1.4.185"
  val mysqlConnectorVersion = "8.0.16"
  Seq(
    "com.typesafe.akka" %% "akka-stream-kafka" % AkkaKafkaStreamVersion,
    "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
    "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
    "com.typesafe.akka" %% "akka-actor" % AkkaVersion,
    "com.typesafe.akka" %% "akka-slf4j" % AkkaVersion,
    "ch.qos.logback" % "logback-classic" % LogbackClassicVersion,
    "com.typesafe.scala-logging" %% "scala-logging" % lazyLoggingVersion,
    "io.circe" %% "circe-core" % circeVersion,
    "io.circe" %% "circe-generic" % circeVersion,
    "io.circe" %% "circe-parser" % circeVersion,
    "org.json4s" %% "json4s-native" % Json4sVersion,
    "org.json4s" %% "json4s-ext" % Json4sVersion,
    "org.scalactic" %% "scalactic" % ScalaTestVersion,
    "org.apache.spark" %% "spark-core" % sparkVersion,
    "org.scalatest" %% "scalatest" % ScalaTestVersion % Test,
    "org.typelevel" %% "cats-core" % CatsVersion,
    "org.typelevel" %% "cats-effect" % CatsEffectVersion withSources() withJavadoc(),
    "org.scalatest" %% "scalatest" % ScalaTestVersion % Test,
    "org.scalacheck" %% "scalacheck" % ScalaCheckVersion % Test,
    "org.tpolecat" %% "doobie-core" % doobieVersion,
    "org.tpolecat" %% "doobie-hikari" % doobieVersion,
    "org.tpolecat" %% "doobie-specs2" % doobieVersion % "test",
    "org.tpolecat" %% "doobie-scalatest" % doobieVersion % "test",
    "com.typesafe.slick" %% "slick" % slickVersion,
    "com.h2database" % "h2" % h2Version,
    "mysql" % "mysql-connector-java" % mysqlConnectorVersion
  )
}

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  "-language:postfixOps",
  "-language:higherKinds",
  "-Ypartial-unification")