package binding

object DebuggableRunner extends App {

  def f(a: Int): Debuggable[Int, String] = Debuggable(a * 2, List(s"f: $a doubled"))

  def g(a: Int): Debuggable[Int, String] = Debuggable(a * 3, List(s"g: $a tripled"))

  def h(a: Int): Debuggable[Int, String] = Debuggable(a * 5, List(s"h: $a times 5"))

  val finalResult = for {
    fRes <- f(23)
    gRes <- g(fRes)
    hRes <- h(gRes)
  } yield hRes

  println(s"Final result on 23: $finalResult")

}
