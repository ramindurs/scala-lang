package binding

/**
  * A Debuggable is a form of a Haskell's Writer monad that carries along some kind of log messages with a value.
  * @param a
  * @param debugs
  * @tparam A the value
  * @tparam B the logs of applying f
  */
case class Debuggable[A, B](a:A, debugs:List[B]) {

  def map[C](f: A => C): Debuggable[C, B] = Debuggable(f(a), debugs)

  def flatMap[C](f: A => Debuggable[C, B]): Debuggable[C, B] = {
    val next = f(a)
    Debuggable(next.a, next.debugs ++ debugs)
  }
}

