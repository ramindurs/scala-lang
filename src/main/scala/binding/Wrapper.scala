package binding

/**
  * By using the companion object with it's apply method, now a value of A can be lifted to a Monad.
  *
  * @param a
  * @tparam A
  */
class Wrapper[A] private(a: A) {

  def map[B](f: A => B): Wrapper[B] = {
    println("In Map: Before executing")
    val rc = f(a)
    println("In Map: After executing but just before returning")
    Wrapper(f(a))
  }

  def flatMap[B](f: A => Wrapper[B]): Wrapper[B] = {
    println("In FlatMap: Before executing")
    val rc = f(a)
    println("In FlatMap: After executing but just before returning")
    rc
  }
}

object Wrapper {
  def apply[A](a: A): Wrapper[A] = new Wrapper(a)
}


object WrapperRunner extends App {

  val a = Wrapper[Int](4)

  val b = Wrapper[Int](12)

  val c = for {
    aValue <- a
    bValue <- b
  } yield aValue + bValue

  println(s"Wrapper value: $c")
}