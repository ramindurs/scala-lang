package binding

object Binding61 extends App {

  def f(i: Int): (String, Int) = if(i == 0) ("Divide By Zero", 0) else ("", 1000 / i)
  def g(i:Int): (String, Int) = if(i == 0) ("Multiplicaton by Zero", 0) else ("", 10 * i)
  def h(i: Int): (String, Int) = if(i == -i) ("Negation", 0) else ("", i - 7)

  val (fLog, a) = f(10)
  val (gLog, b) = g(a)
  val result = (b, s"Log: $fLog $gLog")

  println(s"Initial Binding: $result")

  def bind(func: Int => (String, Int), input: (String, Int)): (String, Int) = {
    val r = f(input._2)
    (s"${input._1} ${r._1}", r._2)
  }

  val r1 = bind(f, ("", 0))
  val r2 = bind(g, r1)
  println(s"Using Binding: $r2")
}
