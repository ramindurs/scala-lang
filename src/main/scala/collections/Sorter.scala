package collections

import scala.collection.immutable.ListMap

object Sorter extends App {

  val votes: Array[String] = Array("Joy", "John", "John", "Billy", "Joy")

  val a: Map[String, Int] = votes.groupBy(identity).mapValues(_.size)



  val b: ListMap[String, Int] = ListMap(a.toSeq.sortWith(_._2 < _._2):_*)
  println(b.last._1)



}
