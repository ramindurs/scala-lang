package scalactic

import org.scalactic._
import TripleEquals._
import StringNormalizations._
import Explicitly._

object ScalacticRunner extends App {

  val aStr = "Hello"
  val bStr = "hello"

  val aRslt = aStr === bStr
  val bRslt = (aStr === bStr) (after being upperCased)
  println(s"Using the ===: $aRslt")
  println(s"Using the ==== and casing: $bRslt")

}
