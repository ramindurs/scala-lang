package transformers

import common.{Address, User}

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object Transformer extends App {

  val a1 = Address(1, "1 Street")
  val a2 = Address(2, "2 Street")
  val u1 = User(1, "John", a1)
  val u2 = User(2, "Tim", a2)

  val result: Future[Unit] = for {
    add1 <- find(1L)
    _ <- Future(println(s"User 1: $add1"))
    add2 <- find(id = 2L)
    _ <- Future(println(s"User 2: $add2"))
    add3 <- find(id = 3L)
    _ <- Future(println(s"User 3: $add3"))
  } yield ()

  Await.ready(result, 5 seconds)

  def find(id: Long): Future[Option[Address]] = findAddressByUserId1(id)

  /**
    * What we really want..
    */
  def findAddressByUserId2(id:Long): Future[Option[Address]] = ???

  /**
    * This is not using transformers. However, it can look a bit ugly.
    */
  def findAddressByUserId1(id: Long): Future[Option[Address]] = {
    findUserById(id).flatMap {
      case Some(user) => findAddressById(user.address.id)
      case _ => Future(None)
    }
  }


  /**
    * Functions that return monads
    */
  def findUserById(id: Long): Future[Option[User]] = {
    id match {
      case 1L => Future {
        Some(u1)
      }
      case 2L => Future(Some(u2))
      case _ => Future {
        None
      }
    }
  }

  def findAddressById(id: Long): Future[Option[Address]] = {
    id match {
      case 1L => Future {
        Some(a1)
      }
      case 2L => Future(Some(a2))
      case _ => Future(None)
    }
  }

}
