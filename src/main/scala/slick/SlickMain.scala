package slick

import com.typesafe.scalalogging.LazyLogging
import slick.basic.DatabasePublisher
import slick.db.{H2Connection, MessageTable}
import slick.dbio.{DBIO, Effect}
import slick.model.Message

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import slick.jdbc.H2Profile.api._
import slick.lifted.MappedProjection
import slick.sql.FixedSqlAction

import scala.util.{Success, Try}

object SlickMain extends App with LazyLogging {

  val create: DBIO[Unit] = H2Connection.dbConnection.createTables()

  val action: Future[Unit] = H2Connection.dbConnection.db.run(create)

  def freshTestData = Seq(
    Message("Dave", "Hello, HAL"),
    Message("HAL", "Hello Dave"),
    Message("Dave", "Open the bay door, HAL"),
    Message("HAL", "I'am sorry, I can't do that.")
  )

  val insert: FixedSqlAction[Option[Int], NoStream, Effect.Write] = H2Connection.dbConnection.messages ++= freshTestData


  val insertResult: Future[Option[Int]] = for {
    _ <- action
    r <- H2Connection.dbConnection.db.run(insert)
  } yield r

  Await.result(insertResult, 10 seconds)

  logger.info(s"INSERT RESULT: [$insertResult]")

  val query: Query[MappedProjection[Quote, (String, Long)], Quote, Seq] = H2Connection.dbConnection.messages.filter(_.sender === "HAL").map(m => (m.content, m.id).mapTo[Quote])

  val hal: Future[Seq[Quote]] = for {
    quotes <-  H2Connection.dbConnection.db.run(query.result)
  } yield quotes

  hal.onComplete {
    case Success(s) => s.foreach(q => logger.info(s"HAL: [${q.content}]"))
    case _ => logger.error("Failed to get HAL's quotes")
  }

  Await.result(hal, 10 seconds)

  val dave: Query[MappedProjection[Quote, (String, Long)], Quote, Seq] = H2Connection.dbConnection.messages.filter(_.sender === "Dave").map(m => (m.content, m.id).mapTo[Quote])

  val daveQuotes: DatabasePublisher[Quote] = H2Connection.dbConnection.db.stream(dave.result)

  daveQuotes.foreach {
    q => logger.info(s"Dave: [${q.content}]")
  }

  Thread.sleep(10000)

}

final case class Quote(content: String, id: Long)
