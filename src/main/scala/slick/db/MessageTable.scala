package slick.db

import slick.jdbc.H2Profile.api._
import slick.lifted.{ProvenShape, Tag}
import slick.model.Message

final class MessageTable(tag: Tag) extends Table[Message](tag, "message") {

  def id: Rep[Long] = column[Long]("id", O.PrimaryKey, O.AutoInc)

  def sender: Rep[String] = column[String]("sender")

  def content: Rep[String] = column[String]("content")

  def * : ProvenShape[Message] = (sender, content, id).mapTo[Message]
}
