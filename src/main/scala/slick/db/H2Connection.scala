package slick.db

import slick.dbio.DBIO
import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._
import slick.jdbc.H2Profile.backend.Database
import slick.lifted.TableQuery

class H2Connection extends DbConnection {

  val db: H2Profile.backend.Database = Database.forConfig("h2slick")

  val messages = TableQuery[MessageTable]

  override def createTables(): DBIO[Unit] = messages.schema.create
}

object H2Connection {

  private val db = new H2Connection

  val dbConnection = db

}
