package slick.db

import slick.dbio.DBIO

trait DbConnection {

  def createTables(): DBIO[Unit]
}
