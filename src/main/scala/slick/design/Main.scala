package slick.design

import slick.design.repository.{DatabaseLayer, DatabaseModule}

object Main extends App {

  val dbLayer: DatabaseModule = new DatabaseModule {
    override val profile = slick.jdbc.H2Profile
  }

  val databaseLayer = new DatabaseLayer(slick.jdbc.H2Profile)
}
