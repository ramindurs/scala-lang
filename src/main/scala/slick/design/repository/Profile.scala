package slick.design.repository

import slick.jdbc.JdbcProfile

trait Profile {

  val profile: JdbcProfile
}

trait DatabaseModule1 { self: Profile =>
  import profile.api._
}

trait DatabaseModule2 { self: Profile =>
  import profile.api._
}

class DatabaseLayer(val profile: JdbcProfile) extends Profile with DatabaseModule1 with DatabaseModule2
