package slick.design.repository

import slick.jdbc.JdbcProfile

trait DatabaseModule {

  val profile:JdbcProfile

  import profile.api._
}
