package nullreplacements

import scala.annotation.tailrec
import scala.util.{Success, Try}

object TryRunner extends App {

  def makeInt(s: String): Try[Int] = Try(s.trim.toInt)

  def sumTry(tryList: Seq[Try[Int]]): Try[Int] = {

    @tailrec
    def loop(tries: Seq[Try[Int]], accum: Int): Try[Int] = {
      tries match {
        case Nil => Success(accum)
        case t :: ts =>
          t match {
            case Success(value) => loop(ts, value + accum)
            case _ => loop(ts, accum)
          }
      }

    }

    loop(tryList, 0)
  }

  def isSuccess[A](t: Try[A]): Boolean = t match {
    case Success(value) => true
    case _ => false
  }


  println("Trying 1:" + makeInt(" 1 "))
  println("Trying 2:" + makeInt("2"))
  println("Trying hello:" + makeInt(" hello "))

  val aList = List("10", "20", "hello")

  val bList = aList.map(makeInt)

  println(s"Printing a list with errors")
  bList.foreach(println)
  val sumWithErrors = sumTry(bList)
  println(s"Sum with errors: $sumWithErrors")

  val cList: Seq[Try[Int]] = for {
    a <- bList
    if isSuccess(a)
  } yield a

  println("Printing a list without errors")
  cList.foreach(println)

  val sumWithoutErrors = sumTry(cList)
  println(s"Sum: $sumWithoutErrors")

}
