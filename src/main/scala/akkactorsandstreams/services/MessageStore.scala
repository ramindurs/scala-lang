package akkactorsandstreams.services

import akka.actor.Actor
import akka.http.scaladsl.model.DateTime
import akkactorsandstreams.model.{Message, Ticker}
import java.time.LocalDateTime

import scala.collection.mutable

case class MessageSummary(id: String, totalSeconds: Int, startTime: LocalDateTime)

object MessageStore {

  val store = new mutable.HashMap[String, MessageSummary]()

  def upsert(m: Message): Unit = {
    store.put(m.id, updateMessage(m, store.get(m.id)))
  }

  def updateMessage(m: Message, maybeSummary: Option[MessageSummary]): MessageSummary =
    maybeSummary
      .map(ms => MessageSummary(ms.id, m.seconds + ms.totalSeconds, ms.startTime))
      .getOrElse(MessageSummary(m.id, m.seconds, m.dateTime))

  def olderThanMin(c: (String, MessageSummary)): Boolean = {
    val cutoff = LocalDateTime.now().minusMinutes(1L)
    val messageTime = c._2.startTime
    cutoff.isAfter(messageTime)
  }

  def removeOldSummary(): Unit = {
    val keys: Iterable[String] = store.filter(c => olderThanMin(c)).keys
    val messages: Iterable[MessageSummary] = for {
      key <- keys
      ms <- store.remove(key)
    } yield (ms)

    messages.foreach(ms => println(s"Old message ${ms}"))
  }

}

class ExtractOldSummary extends Actor {

  override def receive: Receive = {
    case Ticker => {
      MessageStore.removeOldSummary()
    }
    case _ => println("Extractor does not know what you want")
  }
}
