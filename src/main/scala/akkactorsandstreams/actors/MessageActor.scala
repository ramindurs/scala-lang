package akkactorsandstreams.actors

import akka.actor.Actor
import akkactorsandstreams.model.{Message, MessageCreate, MessageUpdate}
import akkactorsandstreams.services.MessageStore

class MessageActor extends Actor {

  override def receive: Receive = {
    case mc: MessageCreate => {
      println(s"New Message received: ${mc}")
      insertRecord(mc)
    }
    case mu: MessageUpdate => {
      println(s"Message Update received: ${mu}")
      updateRecord(mu)
    }
    case _ => println("Unknown message received")
  }


  def insertRecord(mc: MessageCreate): Unit = MessageStore.upsert(mc)

  def updateRecord(mu: MessageUpdate): Unit = MessageStore.upsert(mu)
}

