package akkactorsandstreams

import java.time.LocalDateTime

import akka.actor.{ActorSystem, Props}
import akkactorsandstreams.actors.MessageActor
import akkactorsandstreams.model.{MessageCreate, Ticker}
import akkactorsandstreams.services.ExtractOldSummary

import scala.concurrent.duration._
import scala.language.postfixOps

object ActorSystemRunner extends App {

  val system = ActorSystem("ActorSystem")

  val actor = system.actorOf(Props[MessageActor], "messageActor")
  val ticker = system.actorOf(Props[ExtractOldSummary], "tickerActor")

  val randomNumber = new scala.util.Random

  import system.dispatcher

  system.scheduler.schedule(1 second, 20 second) {
    actor ! MessageCreate(randomNumber.nextInt(5).toString, randomNumber.nextInt(60), LocalDateTime.now())
  }

  system.scheduler.schedule(0 millis, 1 second, ticker, Ticker)

}
