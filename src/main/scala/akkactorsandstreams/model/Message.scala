package akkactorsandstreams.model

import java.time.LocalDateTime
import io.circe._
import io.circe.generic.semiauto._

sealed trait Message {
  val id: String
  val seconds: Int
  val dateTime: LocalDateTime
}

case class MessageCreate(id: String, seconds: Int, dateTime: LocalDateTime) extends Message

case class MessageUpdate(id: String, seconds: Int, dateTime: LocalDateTime) extends Message

object MessageCreate {

  implicit val messageCreateDecoder: Decoder[MessageCreate] = deriveDecoder[MessageCreate]
  implicit val messageCreateEncoder: Encoder[MessageCreate] = deriveEncoder[MessageCreate]
}

object MessageUpdate {
  implicit val messageUpdateDecoder: Decoder[MessageUpdate] = deriveDecoder[MessageUpdate]
  implicit val messageUpdateEncoder: Encoder[MessageUpdate] = deriveEncoder[MessageUpdate]
}
