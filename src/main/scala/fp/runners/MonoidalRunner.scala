package fp.runners

import fp.adt.{Monoid, OptionMonoid}

object MonoidalRunner extends App {

  val stringMonoid = new Monoid[String] {
    override def op(a1: String, a2: String): String = s"$a1$a2"

    override def zero: String = ""
  }

  println(stringMonoid.op("hello", "world"))

  val intOptionMonoid = new OptionMonoid[Int] {
    override def op(a1: Option[Int], a2: Option[Int]): Option[Int] = for {
      a <- a1
      b <- a2
    } yield a + b

    override def zero: Option[Int] = None
  }

  def endoMonoid[A]: Monoid[A => A] = new Monoid[A => A] {
    override def op(a1: A => A, a2: A => A): A => A = a1 andThen a2

    override def zero: A => A = (a:A) => a
  }
}
