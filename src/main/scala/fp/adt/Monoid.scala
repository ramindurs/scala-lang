package fp.adt

trait Monoid[A] {

  def op(a1: A, a2: A): A

  def zero: A
}

trait OptionMonoid[A] extends Monoid[Option[A]]
