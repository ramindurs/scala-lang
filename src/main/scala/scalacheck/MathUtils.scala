package scalacheck

object MathUtils {

  def increaseRandomly(i: Int): Long = {
    i.toLong + getRandomIntFrom1To100
  }

  def getRandomIntFrom1To100: Int = 10

}
