package scalacheck

import scala.annotation.tailrec

object Listutils {

  def dropAllButFirstOccurance[A](list: List[A], a: A): List[A] = {

    @tailrec
    def loop(remaining: List[A], accumulator: List[A], foundFirstOccurance: Boolean): List[A] = {
      remaining match {
        case Nil => accumulator
        case x :: xs => if (x == a && !foundFirstOccurance)
          loop(xs, accumulator ++ List(x), foundFirstOccurance = true)
        else if (x == a && foundFirstOccurance)
          loop(xs, accumulator, foundFirstOccurance)
        else
          loop(xs, accumulator ++ List(x), foundFirstOccurance)
      }
    }
    loop(list, List(), foundFirstOccurance = false)
  }
}
