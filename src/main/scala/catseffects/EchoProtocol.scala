package catseffects

import java.io.{BufferedReader, BufferedWriter, InputStreamReader, PrintWriter}
import java.net.{ServerSocket, Socket}

import cats.effect.ExitCase.{Canceled, Completed, Error}
import cats.effect.concurrent.MVar
import cats.effect.{Concurrent, Resource, Sync}
import cats.implicits._
import cats.effect.syntax.all._

class EchoProtocol {

  def echoProtocol[F[_] : Sync](clientSocket: Socket, stopFlag: MVar[F, Unit]): F[Unit] = {

    def createResources(socket: Socket): Resource[F, (BufferedReader, BufferedWriter)] =
      for {
        readerResource <- createReaderResource(socket)
        writerResource <- createWriterResource(socket)
      } yield (readerResource, writerResource)

    def createReaderResource(socket: Socket): Resource[F, BufferedReader] =
      Resource.make {
        Sync[F].delay(new BufferedReader(new InputStreamReader(socket.getInputStream)))
      } { reader =>
        Sync[F].delay(reader.close()).handleErrorWith(_ => Sync[F].unit)
      }

    def createWriterResource(socket: Socket): Resource[F, BufferedWriter] =
      Resource.make {
        Sync[F].delay(new BufferedWriter(new PrintWriter(socket.getOutputStream)))
      } {
        writer =>
          Sync[F].delay(writer.close()).handleErrorWith(_ => Sync[F].unit)
      }

    def loop(reader: BufferedReader, writer: BufferedWriter, stopFlag: MVar[F, Unit]): F[Unit] =
      for {
        line <- Sync[F].delay(reader.readLine())
        _ <- line match {
          case "" => Sync[F].unit
          case "STOP" => stopFlag.put(())
          case _ => Sync[F].delay(writeToSocket(line, writer)) >> loop(reader, writer, stopFlag)
        }
      } yield ()

    def writeToSocket(line: String, writer: BufferedWriter): Unit = {
      writer.write(s"ECHO: $line")
      writer.newLine()
      writer.flush()
    }

    /*
     * When using Cats Effects, it really has the following structure:
     * 1. Create the Resources (createResources in this case)
     * 2. Use the resources (loop in this case)
     */
    createResources(clientSocket).use {
      case (reader, writer) => loop(reader, writer, stopFlag:MVar[F, Unit])
    }
  }

  def serve[F[_] : Concurrent](serverSocket: ServerSocket, stopFlag: MVar[F, Unit]): F[Unit] = {

    def close(socket: Socket): F[Unit] =
      Sync[F].delay(socket.close()).handleErrorWith(_ => Sync[F].unit)

    for {
      _ <- Sync[F]
        .delay(serverSocket.accept())
        .bracketCase { useSocket =>
          println(s"Accepting connection on ${useSocket.toString}")
          echoProtocol(useSocket, stopFlag)
            .guarantee(close(useSocket))
            .start >> Sync[F].pure(useSocket)
        } {
          (socket, exit) =>
            exit match {
              case Completed => Sync[F].unit
              case Error(_) | Canceled => close(socket)
            }
        }
      _ <- serve(serverSocket, stopFlag)
    } yield ()

  }
}
