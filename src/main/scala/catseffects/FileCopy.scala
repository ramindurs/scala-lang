package catseffects

import java.io.{File, FileInputStream, FileOutputStream, InputStream, OutputStream}

import cats.effect.concurrent.Semaphore
import cats.effect.{Concurrent, ExitCode, IO, IOApp, Resource}
import com.typesafe.scalalogging.LazyLogging
import cats.implicits._

object FileCopy extends IOApp with LazyLogging {

  def copy(origin: File, dest: File, buffer: Array[Byte])(implicit concurrent: Concurrent[IO]): IO[Long] =
    for {
      guard <- Semaphore[IO](1)
      count <- inOutStreams(origin, dest, guard).use {
        case (in, out) => guard.withPermit(transfer(in, out, buffer))
      }
    } yield count

  def transfer(orgin: InputStream, dest: OutputStream, buffer: Array[Byte]): IO[Long] =
    for {
      total <- transmit(orgin, dest, buffer, 0L)
    } yield total

  def transmit(orgin: InputStream, dest: OutputStream, buffer: Array[Byte], acc: Long): IO[Long] = {
    for {
      amount <- IO(orgin.read(buffer, 0, buffer.length))
      count <- if (amount > -1) {
        IO(dest.write(buffer, 0, amount)) >> transmit(orgin, dest, buffer, acc + amount)
      }
      else
        IO.pure(acc)
    } yield count
  }

  def inOutStreams(in: File, out: File, guard: Semaphore[IO]): Resource[IO, (InputStream, OutputStream)] =
    for {
      i <- inputStream(in, guard)
      o <- outputStream(out, guard)
    } yield (i, o)

  // Resource alls us to orderly create, use and then release resources.
  // We could have used Resource.fromAutoCloseable, but then we would loose control over what happens when
  // exceptions are thrown.
  def inputStream(f: File, guard: Semaphore[IO]): Resource[IO, FileInputStream] =
    Resource.make {
      IO(new FileInputStream(f))
    } {
      i =>
        guard.withPermit {
          IO(i.close()).handleErrorWith {
            _ => IO.unit
          }
        }
    }

  def outputStream(f: File, guard: Semaphore[IO]): Resource[IO, FileOutputStream] =
    Resource.make {
      IO(new FileOutputStream(f))
    } {
      o =>
        guard.withPermit {
          IO(o.close()).handleErrorWith {
            _ => IO.unit
          }
        }
    }

  override def run(args: List[String]): IO[ExitCode] = {
    for {
      buffer <- IO(new Array[Byte](1024 * 10))
      _ <- IO(println("Copying files"))
      origin = new File("/opt/spark-data/data.csv")
      dest = new File(("/opt/spark-data/NewData.csv"))
      count <- copy(origin, dest, buffer)
      _ <- IO(println(s"Copied $count bytes from ${origin.getPath} to ${dest.getPath}"))
    } yield ExitCode.Success
  }
}
