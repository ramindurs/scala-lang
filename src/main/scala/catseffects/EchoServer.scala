package catseffects

import java.net.{ServerSocket, Socket}

import cats.effect.concurrent.MVar
import cats.implicits._
import cats.effect.syntax.all._
import cats.effect.{Concurrent, ExitCode, IO, IOApp, Sync}

object EchoServer extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {

    val protocol = new EchoProtocol()

    def close[F[_] : Sync](socket: ServerSocket): F[Unit] =
      Sync[F].delay(socket.close()).handleErrorWith(_ => Sync[F].unit)

    def server[F[_] : Concurrent](serverSocket: ServerSocket): F[ExitCode] =
      for {
        stopFlag <- MVar[F].empty[Unit]
        serverFiber <- protocol.serve(serverSocket, stopFlag).start
        _ <- stopFlag.read
        _ <- serverFiber.cancel.start
      } yield ExitCode.Success

    IO(new ServerSocket(args.headOption.map(_.toInt).getOrElse(5432)))
      .bracket {
        svrSocket => server[IO](svrSocket) >> IO.pure(ExitCode.Success)
      } {
        svrSocket => close[IO](svrSocket) >> IO(println("Server Finished Serving"))
      }
  }
}
