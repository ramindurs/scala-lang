package kafka

import akka.Done
import akka.actor.ActorSystem
import akka.kafka.ConsumerMessage.CommittableMessage
import akka.kafka.{ConsumerMessage, ConsumerSettings, Subscriptions}
import akka.stream.ActorMaterializer
import akka.kafka.scaladsl.Consumer
import akka.stream.scaladsl.{Keep, RunnableGraph, Sink, Source}
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

object KafkaConsumer extends App {

  implicit val system: ActorSystem = ActorSystem("KafkaClient")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContextExecutor = system.dispatcher

  val commitableSource: Source[ConsumerMessage.CommittableMessage[String, String], Consumer.Control] =
    Consumer.committableSource(KafkaConfig.consumerSettings, Subscriptions.topics(KafkaConfig.topics: _*))

  def streamingApplication: RunnableGraph[(Consumer.Control, Future[Done])] = commitableSource
    .mapAsync(1) { message: CommittableMessage[String, String] =>
      dealWithMessage(message).map(_ => message.committableOffset.commitScaladsl())
    }
    .toMat(Sink.ignore)(Keep.both)

  def dealWithMessage(message: ConsumerMessage.CommittableMessage[String, String]): Future[Done.type] = {
    println(s"Receiving ${message.record.topic()}\t${message.record.timestamp()}\t${message.record.value()}\t${message}")
    Future(Done)
  }

  val (control, future) = streamingApplication.run()
  future onComplete {
    case Success(value) => println("Future completed")
    case Failure(exception) => exception.printStackTrace()
  }

  control.isShutdown onComplete {
    case Success(value) => println("Shutdown is completed")
    case Failure(exception) => exception.printStackTrace()
  }

}

object KafkaConfig {
  val inputTopic = "test-topic"
  val topics = Seq(inputTopic)

  val stringDeserializer = new StringDeserializer

  def consumerSettings(implicit as: ActorSystem): ConsumerSettings[String, String] =
    ConsumerSettings(as, stringDeserializer, stringDeserializer)
      .withBootstrapServers("localhost:9093")
      .withGroupId("scala-lang-group-1")
      .withProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false")
}
