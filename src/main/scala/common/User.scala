package common

case class User(id: Long, name: String, address: Address)

case class Address(id: Long, street: String)
