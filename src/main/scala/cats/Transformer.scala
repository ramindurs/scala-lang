package cats

import cats.data.OptionT
import cats.implicits._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.language.postfixOps
import scala.util.Success
import scala.concurrent.duration._


object Transformer extends App {

  val aResult: Future[Option[Int]] = Future.successful(Some(10))

  val a: OptionT[Future, Int] = OptionT(aResult)

  def double(a: Int): Future[Option[Int]] = Future.successful(Some(a * 2))

  val b: OptionT[Future, Int] = for {
    i <- a
    d <- OptionT(double(i))
  } yield d

  val c: Future[Option[Int]] = b.value

  c.onComplete {
    case Success(Some(value)) => println(s"Result: $value")
    case _ => println("No result")
  }

  Await.result(c, 5 seconds)

}
