package cats

import cats.implicits._

object Lifter extends App {

  val reciprical: PartialFunction[Int, Double] = {
    case aNumber if aNumber != 0 => "1".toDouble / aNumber.toDouble
  }

  val quarter: Double = reciprical(4)
  println(s"Quarter: $quarter")

  //val boom = reciprical(0)
  //println(s"Boom! $boom")

  val lifted = reciprical.lift
  val aBoom: Option[Double] = lifted(0)
  aBoom match {
    case Some(value) => println(s"Recipricol: $value")
    case None => println("You tried to divide by 0")
  }

}
