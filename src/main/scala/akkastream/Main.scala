package akkastream

import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl._
import akka.{Done, NotUsed}

import scala.concurrent._

object Main extends App {

  implicit val system: ActorSystem = ActorSystem("AkkaStream")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContextExecutor = system.dispatcher

  val source: Source[Int, NotUsed] = Source(1 to 100)

  val done: Future[Done] = source.runForeach(handleSourceEmition)

  done.onComplete(_ => system.terminate())


  def handleSourceEmition(i: Int) : Unit = {
    println(i)
  }
}
