##To POST:
`curl -v -H "Content-Type: application/json" \
   -X POST http://localhost:8080/questions \
   -d '{"id": "test", "title": "MyTitle", "text":"The text of my question"}'`

According to the REST protocol, a POST request should reply with a 201 (Created). Also, a <i>Location Header</i> with 
the URI that identifies the location of the new entity should be returned. If the request already exists, then it should
return 409 (Conflict).
