package akkahttp.entities

case class Question(id: String, title: String, text: String)