package akkahttp.serializers

import java.text.SimpleDateFormat
import org.json4s.ext.JodaTimeSerializers
import org.json4s.{native, DefaultFormats, Formats}

trait JsonSupport extends {

  implicit val serialization = native.Serialization

  implicit def json4sFormats: Formats = customDateFormat ++ JodaTimeSerializers.all ++ CustomSerializers.all

  val customDateFormat = new DefaultFormats {
    override def dateFormatter: SimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
  }

}
