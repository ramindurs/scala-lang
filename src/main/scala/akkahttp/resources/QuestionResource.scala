package akkahttp.resources

import akka.http.scaladsl.server.Route
import akkahttp.entities.Question
import akkahttp.routing.MyResource
import akkahttp.services.QuestionService

trait QuestionResource extends MyResource {

  val questionService: QuestionService

//  def questionRoutes: Route = pathPrefix("questions"){
//    pathEnd {
//      post {
//        entity(as[Question]) {question =>
//          completeWithLocationHeader(resourceId = questionService.createQuestion(question), ifDefinedStatus = 201, ifEmptyStatus = 409)
//        }
//      }
//    }
//  }
}
