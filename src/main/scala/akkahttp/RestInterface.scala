package akkahttp

import akka.http.scaladsl.server.Route
import akkahttp.resources.QuestionResource
import akkahttp.services.QuestionService

import scala.concurrent.ExecutionContext

trait RestInterface extends Resources {
  implicit def executionContext: ExecutionContext

  lazy val questionService = new QuestionService

  //val routes: Route = questionRoutes
}

trait Resources extends QuestionResource
