package challenges.springer.input

import java.io.InputStream

import scala.io.StdIn

trait Reader{
  def readLine(): String
}

class StdReader(in:InputStream) extends Reader {

  def this() = {
    this(System.in)
  }

  override def readLine(): String = {
    print(s"enter command: ")
    StdIn.readLine()
  }
}
