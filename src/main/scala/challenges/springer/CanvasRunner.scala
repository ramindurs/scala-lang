package challenges.springer

import challenges.springer.canvas.Canvas
import challenges.springer.command.{Command, Error, Quit, Start}
import challenges.springer.input.Reader
import challenges.springer.processor.Processor

import scala.annotation.tailrec

object CanvasRunner {

  @tailrec
  def run(canvas: Canvas, command: Command)(implicit reader: Reader, proc: Processor): Canvas = {
    command match {
      case Quit() => canvas
      case Error(msg) =>
        println(msg)
        run(canvas, Command.from(reader.readLine()))
      case Start() => run(canvas, Command.from(reader.readLine()))
      case _ =>
        val updatedCanvas = proc.update(canvas, command)
        updatedCanvas.canvas.foreach(row => row.foreach(print))
        run(updatedCanvas, Command.from(reader.readLine()))
    }

  }
}
