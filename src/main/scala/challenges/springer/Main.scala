package challenges.springer

import challenges.springer.canvas.Canvas
import challenges.springer.command.Start
import challenges.springer.input.StdReader
import challenges.springer.processor.Processor

object Main extends App {

  println(s"Starting.... ")

  implicit val reader = new StdReader()
  implicit val proc = new Processor()

  CanvasRunner.run(Canvas(0, 0), Start())

  println(s"Finishing....")

}
