package challenges.springer.canvas

import challenges.springer.command.{BucketFill, Command, Line, Rectangle}

import scala.annotation.tailrec

class Canvas(val canvas: List[List[String]]) {

  def drawBox(r: Rectangle): Canvas = {
    val top = drawLine(Line(r.x1, r.y1, r.x2, r.y1))
    val bottom = top.drawLine(Line(r.x1, r.y2, r.x2, r.y2))
    val left = bottom.drawLine(Line(r.x1, r.y1, r.x1, r.y2))
    left.drawLine(Line(r.x2, r.y1, r.x2, r.y2))
  }

  def drawLine(line: Line): Canvas = {
    @tailrec
    def loop(y: Int, unmodified: List[List[String]], acc: List[List[String]]): List[List[String]] = {
      unmodified match {
        case Nil => acc
        case row :: rows =>
          val modfied = modifyRow(y, row, line)
          loop(y + 1, rows, acc ++ List(modfied))
      }
    }

    new Canvas(loop(0, canvas, List()))
  }

  private def modifyRow(y: Int, row: List[String], line: Line): List[String] = {
    @tailrec
    def loop(x: Int, unmodified: List[String], modified: List[String]): List[String] = {
      unmodified match {
        case Nil => modified
        case cell :: cells =>
          if (line.shouldDraw(x, y))
            loop(x + 1, cells, modified ++ List("x"))
          else
            loop(x + 1, cells, modified ++ List(cell))
      }
    }

    loop(0, row, List())
  }

  def fill(b: BucketFill): Canvas = {
    val mappedCanvas = generateMappedCanvas(canvas)

    @tailrec
    def loop(y: Int, unprocessed: List[List[String]], acc: List[List[String]], updatedMap: Map[(Int, Int), String]): (List[List[String]], Map[(Int, Int), String]) = {
      unprocessed match {
        case Nil => (acc, updatedMap)
        case row :: rows =>
          val updatedRowAndMap = fillRow(b, y, row, updatedMap)
          loop(y + 1, rows, acc ++ List(updatedRowAndMap._1), updatedRowAndMap._2)
      }
    }

    if (mappedCanvas.get(b.x, b.y).contains(" ")) {
      val updated = loop(0, canvas, List(), mappedCanvas ++ Map((b.x, b.y) -> b.c.toString))
      val reversed = reverseCanvas(updated._1)
      val reversedMap = generateMappedCanvas(reversed)
      val updatedReversed = loop(0, reversed, List(), reversedMap)
      new Canvas(reverseCanvas(updatedReversed._1))
    } else
      this
  }

  private def fillRow(fill: BucketFill, y: Int, row: List[String], canvasMap: Map[(Int, Int), String]): (List[String], Map[(Int, Int), String]) = {

    @tailrec
    def loop(x: Int, unprocessed: List[String], acc: List[String], updatedMap: Map[(Int, Int), String]): (List[String], Map[(Int, Int), String]) = {
      unprocessed match {
        case Nil => (acc, updatedMap)
        case cell :: cells =>
          if (isCellEmpty(x, y, updatedMap) && isAdjacentToFiller(x, y, fill.c.toString, updatedMap))
            loop(x + 1, cells, acc ++ List(fill.c.toString), updatedMap ++ Map((x, y) -> fill.c.toString))
          else
            loop(x + 1, cells, acc ++ List(cell), updatedMap)
      }
    }

    loop(0, row, List(), canvasMap)
  }

  private def isCellEmpty(x: Int, y: Int, canvasMap: Map[(Int, Int), String]): Boolean = canvasMap.get((x, y)).contains(" ")

  private def isAdjacentToFiller(x: Int, y: Int, fillChar: String, canvasMap: Map[(Int, Int), String]): Boolean = {
    checkAt(x - 1, y - 1, fillChar, canvasMap) ||
      checkAt(x, y - 1, fillChar, canvasMap) ||
      checkAt(x + 1, y - 1, fillChar, canvasMap) ||
      checkAt(x - 1, y, fillChar, canvasMap) ||
      checkAt(x + 1, y, fillChar, canvasMap) ||
      checkAt(x - 1, y + 1, fillChar, canvasMap) ||
      checkAt(x, y + 1, fillChar, canvasMap) ||
      checkAt(x + 1, y + 1, fillChar, canvasMap)
  }

  private def checkAt(x: Int, y: Int, fillChar: String, canvasMap: Map[(Int, Int), String]): Boolean = canvasMap.get(x, y).contains(fillChar)

  private def reverseCanvas(cnvs: List[List[String]]): List[List[String]] = {
    val reversedY = cnvs.reverse
    reversedY.map(row => row.reverse)
  }

  private def generateMappedCanvas(cnvs: List[List[String]]): Map[(Int, Int), String] = {
    @tailrec
    def loop(unprocessed: List[List[String]], y: Int, acc: Map[(Int, Int), String]): Map[(Int, Int), String] = {
      unprocessed match {
        case Nil => acc
        case row :: rows =>
          val processedRow = processRow(y, row)
          loop(rows, y + 1, acc ++ processedRow)
      }
    }

    loop(cnvs, 0, Map())
  }

  private def processRow(y: Int, row: List[String]): Map[(Int, Int), String] = {
    @tailrec
    def loop(x: Int, unprocessed: List[String], acc: Map[(Int, Int), String]): Map[(Int, Int), String] = {
      unprocessed match {
        case Nil => acc
        case cell :: cells =>
          loop(x + 1, cells, acc ++ Map((x, y) -> cell))
      }
    }

    loop(0, row, Map())
  }
}

object Canvas {

  def apply(width: Int, height: Int): Canvas = {
    val topLine = List.fill(width + 2)("-") ++ List("\n")
    val bottomLine = List.fill(width + 2)("-") ++ List("\n")
    new Canvas(List(topLine) ++ getMiddleSection(width, height) ++ List(bottomLine))
  }

  private def getMiddleSection(width: Int, height: Int): List[List[String]] = {

    def loop(count: Int, acc: List[List[String]]): List[List[String]] = {
      if (count <= 0)
        acc
      else
        loop(count - 1, acc ++ List(getMiddleRow(width)))
    }

    def getMiddleRow(width: Int): List[String] = List("|") ++ List.fill(width)(" ") ++ List("|\n")

    loop(height, List())
  }
}
