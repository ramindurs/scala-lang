package challenges.springer.processor

import challenges.springer.canvas.Canvas
import challenges.springer.command.{BucketFill, Command, CreateCanvas, Line, Rectangle}

class Processor {

  def update(canvas: Canvas, command: Command): Canvas = {
    command match {
      case CreateCanvas(width, hight) => Canvas(width, hight)
      case l@Line(x1, y1, x2, y2) => canvas.drawLine(l)
      case r@Rectangle(x1, y1, x2, y2) => canvas.drawBox(r)
      case b@BucketFill(x, y, c) => canvas.fill(b)
      case _ => canvas
    }
  }

}
