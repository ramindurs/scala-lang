package challenges.springer.command

sealed trait Command{
  def shouldDraw(x: Int, y: Int): Boolean

  def onSameX(x1: Int, x2:Int, x: Int): Boolean = x1 == x2 && x1 == x

  def onSameY(y1: Int, y2: Int, y: Int): Boolean = y1 == y2 && y1 == y

  def isBetweenOnAxis(p1: Int, p2: Int, p: Int): Boolean = {
    if(p1 == p2 && p1 == p)
      true
    else if(p1 > p2 && p1 >= p && p >= p2)
      true
    else if(p1 < p2 && p1 <= p && p <= p2)
      true
    else
      false
  }


}

case class Start() extends Command {
  override def shouldDraw(x: Int, y: Int): Boolean = false
}

case class CreateCanvas(width: Int, hight: Int) extends Command {
  override def shouldDraw(x: Int, y: Int): Boolean = false
}

case class Line(x1: Int, y1: Int, x2: Int, y2: Int) extends Command {
  override def shouldDraw(x: Int, y: Int): Boolean = {
    (onSameY(y1, y2, y) && isBetweenOnAxis(x1, x2, x)) || (onSameX(x1, x2, x) &&isBetweenOnAxis(y1, y2, y))
  }
}

case class Rectangle(x1: Int, y1: Int, x2: Int, y2: Int) extends Command {
  override def shouldDraw(x: Int, y: Int): Boolean = false
}

case class BucketFill(x: Int, y: Int, c: Char) extends Command {
  override def shouldDraw(x: Int, y: Int): Boolean = false
}

case class Quit() extends Command {
  override def shouldDraw(x: Int, y: Int): Boolean = false
}

case class Error(msg: String) extends Command {
  override def shouldDraw(x: Int, y: Int): Boolean = false
}


object Command {

  implicit class Regex(sc: StringContext) {
    def r = new util.matching.Regex(sc.parts.mkString, sc.parts.tail.map(_ => "x"): _*)
  }

  def from(instruction: String): Command =
    instruction match {
      case r"C (\d+)$w (\d+)$h" => CreateCanvas(w.toInt, h.toInt)
      case r"L (\d+)$x1 (\d+)$y1 (\d+)$x2 (\d+)$y2" => Line(x1.toInt, y1.toInt, x2.toInt, y2.toInt)
      case r"R (\d+)$x1 (\d+)$y1 (\d+)$x2 (\d+)$y2" => Rectangle(x1.toInt, y1.toInt, x2.toInt, y2.toInt)
      case r"B (\d+)$x (\d+)$y (\D)$c" => BucketFill(x.toInt, y.toInt, c.charAt(0))
      case "Q" => Quit()
      case _ => Error(s"Instruction [$instruction] not supported")
    }

}



