package games.cointoss

import java.io.{InputStream, OutputStream}
import java.util.Scanner

object InteractiveReader {

  def readString(in: InputStream, out: OutputStream, msg: String): String = {
    out.write(msg.getBytes)

    val scanner = new Scanner(in)
    scanner.next
  }
}
