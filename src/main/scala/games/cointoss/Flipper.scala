package games.cointoss

import games.cointoss.Coin.{HEAD, Side, TAIL}

import scala.util.Random

object Coin {
  sealed abstract class Side()
  case object HEAD extends Side()
  case object TAIL extends Side()
}


class Flipper(random: Random){


  private val correct = "Your guess was correct. "
  private val incorrect = "You made an incorrect guess. "

  def flipCoinAndCheck(guess: Side, game: Game):(String, Game) = {
    val coinSide = flipCoin()
    if (guess == coinSide)
      (correct, Game(game.correct + 1, game.turns + 1))
    else
      (incorrect, Game(game.correct, game.turns + 1))
  }

  def flipCoin():Side = {
    val num = random.nextInt(2)
    if(num == 0)
      TAIL
    else
      HEAD
  }

}

object Flipper {

  def get(): Flipper = new Flipper(new Random())
  def get(seed: Int) = new Flipper(new Random(seed))
}
