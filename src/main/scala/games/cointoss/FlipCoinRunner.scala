package games.cointoss

import games.cointoss.Coin.{HEAD, TAIL}

import scala.annotation.tailrec

case class Game(correct: Int, turns: Int)

object FlipCoinRunner extends App {

  val msg = "H(ead), T(ail) or Q(uit):"
  val errorMsg = "Try again using one of the following first letter " + msg
  val standardInteractive = (str: String) => InteractiveReader.readString(System.in, System.out, str)
  val randomFlipper = Flipper.get()

  @tailrec
  def loop(choice: String, game: Game, flipper: Flipper): Game = choice match {
    case "Q" => game
    case "H" =>
      val result: (String, Game) = flipper.flipCoinAndCheck(HEAD, game)
      loop(standardInteractive(result._1 + msg), result._2, flipper)
    case "T" =>
      val result = flipper.flipCoinAndCheck(TAIL, game)
      loop(standardInteractive(result._1 + msg), result._2, flipper)
    case _ => loop(standardInteractive(errorMsg), game, flipper)
  }

  val results = loop(standardInteractive(msg), Game(0, 0), randomFlipper)

  println(s"Final result: $results")

}
