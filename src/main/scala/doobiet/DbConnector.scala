package doobiet

import cats.effect.{ContextShift, IO}
import doobie._
import doobie.util.transactor.Transactor.Aux

import scala.concurrent.ExecutionContext

object DbConnector{

  implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContext.global)

  val basexa: Aux[IO, Unit] = Transactor.fromDriverManager[IO](
    "com.mysql.cj.jdbc.Driver",
    "jdbc:mysql://localhost:3306/scalalang?useSSL=false",
    "scala", "password"
  )
}
