package concurrent

import java.util.concurrent.ConcurrentHashMap
import scala.collection.JavaConverters._

class ConcurrentMapRunner {

  private val map = new ConcurrentHashMap[String, String]()

  def put(key: String, value: String): String = map.put(key, value)


  def get(key: String): String = map.get(key)

  def getKeys() = map.keySet().iterator().asScala.toList
}
