package scalalang.typeclass

object OrderingExample extends App {

  val minOrdering: Ordering[Int] = Ordering.fromLessThan[Int](_ < _)

  val sorted =  List(3,4,2).sorted

  println(sorted)
}
