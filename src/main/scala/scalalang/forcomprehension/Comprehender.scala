package scalalang.forcomprehension

object Comprehender extends App {

  val y3: Perhaps[Int] = YestItIs(3)
  val y4: Perhaps[Int] = YestItIs(4)
  val y6: Perhaps[Int] = YestItIs(6)
  val nope = Nope


  /**
    * This will fail when there is not a foreach method on the class.
    * The function println is sent to the foreach method,
    * y3.foreach(a => println(a))
    * Nope will not print anything.
    */
  for {
    a <- y3
  } println(a)

  /**
    * This will fail when there is no map method.
    * The yield will apply the function to the map method of y3,
    * .map(a => a * a)
    */
  val square: Perhaps[Int] = for {
    a <- y3
  } yield a * a
  println(s"Square: ${square}")

  /**
    * This will fail when there is no flatmap method.
    * The yield will apply flatmap to y3 and then apply the map to y4,
    * .flatmap(a => y4.map => a * B)
    */
  val multiple: Perhaps[Int] = for {
    a <- y3
    b <- y4
  } yield a * b
  println(s"Multiple: ${multiple}")

  /**
    * y3.flatMap(a => y4.map(b => a + b).map(d => y5.map(c => c + d)))
    */
  val sum = for {
    a <- y3
    b <- y4
    c <- y6
  } yield a + b + c
  println(s"Sum: ${sum}")

  /**
    * This will fail when there is no withFilter method.
    * The if statement uses the y3.withFilter and them maps it,
    * y3.withFilter(a => a != 0).flatMap(a => y6.map(b => b/a))
    */
  val div = for {
    a <- y3
    if a != 0
    b <- y6
  } yield b / a
  println(s"Divisor: ${div}")



}
