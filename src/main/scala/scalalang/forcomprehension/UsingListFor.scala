package scalalang.forcomprehension

object UsingListFor extends App {

  val xs = List(2, 4, 6)
  val ys = List(100,200, 300)
  val zs = List(1000, 2000, 3000)

  val permutations = for {
    x <- xs
    y <- ys
    z <- zs
  }yield x + y + z

  println(s"$permutations")

}
