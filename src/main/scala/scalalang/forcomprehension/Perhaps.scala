package scalalang.forcomprehension

sealed abstract class Perhaps[+A] {
  // A foreach that takes a function and returns unit - required for a IO
  def foreach(f: A => Unit): Unit

  // yield supplies the function to the map method
  def map[B](f: A => B): Perhaps[B]

  // yield supplies the function to the flatMap
  def flatMap[B](f: A => Perhaps[B]): Perhaps[B]

  def withFilter(f: A => Boolean): Perhaps[A]
}

case class YestItIs[A] (value: A) extends Perhaps[A] {

  override def foreach(f: A => Unit): Unit = f(value)

  override def map[B](f: A => B): Perhaps[B] = YestItIs(f(value))

  override def flatMap[B](f: A => Perhaps[B]): Perhaps[B] = f(value)

  override def withFilter(f: A => Boolean): Perhaps[A] = if(f(value)) this else Nope
}

case object Nope extends Perhaps[Nothing] {

  override def foreach(f: Nothing => Unit): Unit = ()

  override def map[B](f: Nothing => B): Perhaps[B] = this

  override def flatMap[B](f: Nothing => Perhaps[B]): Perhaps[B] = this

  override def withFilter(f: Nothing => Boolean): Perhaps[Nothing] = this
}