package scalalang.traitsf

/**
  * PRODUCT type
  * A TrafficLight is either red, amber or green.
  * Below models a "is-a or" relationship
  */
sealed trait TrafficLight

case object Red extends TrafficLight
case object Amber extends TrafficLight
case object Green extends TrafficLight


sealed trait Source
case object Well extends Source
case object Spring extends Source
case object Tap extends Source


/**
  * SUM type
  * Water has size, source and carbonated, "has-a and" relationship.
  * Below models this with source "is-a or"
  *
 */
case class Water(size:Int, source:Source, carbonated: Boolean)