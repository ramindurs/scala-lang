package scalalang.traitsf

sealed trait Food

case object Meat extends Food


/**
  * The value of sounce depends upon the type of instance created. This is an example of structural recursion using
  * polymorphism.
  */
sealed trait Feline {
  def colour:String
  def sound:String


  /**
    * Below is an example of structural recursion using pattern matching.
    * @return
    */
  def dinner:Food =
    this match {
      case Cat(s,f) => f
      case Lion(c,i) => Meat
    }
}

case class Cat(colour:String, favouriteFood:Food) extends Feline {
  val sound = "meow"
}

case class Lion(colour:String, maneSize:Int) extends Feline {
  val sound = "roar"
}

