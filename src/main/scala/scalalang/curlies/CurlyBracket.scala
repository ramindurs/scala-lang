package scalalang.curlies

// These require a new keyword and could have been defined as trait or abstract classes
trait X {
  def printer(s: String): Unit
}

trait Person {
  def name: String

  def age: Int

  override def toString: String = s"Name: $name; Age: $age"
}

// Class that takes a single argument function, called a.
case class F(a: String => Int)

case class S[A, B](cal: (A, A) => B)

object CurlyBracket extends App {

  // These are examples of anonymous class
  val x = new X {
    override def printer(s: String): Unit = println(s"$s")
  }
  x.printer("Hello")

  val tom = new Person {
    val name = "Tom"
    val age = 22
  }
  println(s"$tom")

  // A function that takes a by-name parameter - the argument is the thunk
  val y = timer {
    Thread.sleep(1000)
    42
  }
  println(s"Result of By-Name Function: ${y._1}, ${y._2}")

  // A class that takes a function parameter. This could be a case class or a function
  val f = F {
    (a: String) => a.length
  }
  val result = f.a("Banana")
  println(s"Banana size $result")

  val s = S {
    (x: Int, y: Int) => x.toDouble / y
  }

  println("Division: " + s.cal(5,3))

  def timer[A](thunk: => A): (A, Long) = {
    val startTime = System.nanoTime()
    val result = thunk
    val stopTime = System.nanoTime()
    (result, (stopTime - startTime) / 1000)
  }

}


