package scalalang.patternmatching

import java.time.{LocalDate, LocalDateTime, LocalTime}

object DateTime {

  def unapply(dt: LocalDateTime): Some[(LocalDate, LocalTime)] =
    Some((dt.toLocalDate, dt.toLocalTime))

}

object Date {
  def unapply(d: LocalDate): Some[(Int, Int, Int)] =
    Some(d.getYear, d.getMonthValue, d.getDayOfMonth)
}

object Time {
  def unapply(t: LocalTime): Some[(Int, Int, Int)] =
    Some(t.getHour, t.getMinute, t.getSecond)
}

object DateTimeSeq {
  def unapplySeq(dt: LocalDateTime): Some[Seq[Int]] =
    Some(Seq(
      dt.getYear, dt.getMonthValue, dt.getDayOfMonth,
      dt.getHour, dt.getMinute, dt.getSecond))
}

object AM {
  def unapply(t: LocalTime): Option[(Int, Int, Int)] =
    t match {
      case Time(h, m, s) if h < 12 => Some((h, m, s))
      case _ => None
    }
}

object PM {
  def unapply(t: LocalTime): Option[(Int, Int, Int)] =
    t match {
      case Time(12, m, s) => Some((12, m, s))
      case Time(h, m, s) if h > 12 => Some((h - 12, m, s))
      case _ => None
    }
}