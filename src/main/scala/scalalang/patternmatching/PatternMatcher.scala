package scalalang.patternmatching
import java.time.{LocalDate, LocalDateTime, LocalTime}

import scala.collection.immutable

object PatternMatcher extends App {

  val me = FullName("John", "Smith")

  val FullName(meFirst, meLast) = me
  println(s"${me} = $meFirst $meLast")

  val meFor: String = for {
    a <- me.first
  } yield a
  println(s"MeFor: ${meFor}")

  val Date(year, month, day) = LocalDate.now()
  println(s"Year: $year, Month: $month, Day: $day")

  val dt @ DateTime(date @ Date(y, m, d), time @ Time(h, mm, s))= LocalDateTime.now()
  println(s"$y, $m, $d, : $h, $mm, $s")
  println(s"DateTime: $dt; Date: $date; Time: $time")

  val dtSeq @ DateTimeSeq(_*) = LocalDateTime.now()
  println(s"Date time sequence: $dtSeq")

  val am @ AM(amh, amm, _) = LocalTime.now()
  val pm @ PM(pmh, pmm, _) = LocalTime.now()
  println(s"AM: $am; PM: $pm")
}
