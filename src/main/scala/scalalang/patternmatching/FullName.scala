package scalalang.patternmatching

/**
  * Removal of case class declaration means that the two arguments must be declared as val
  * @param first
  * @param last
  */
class FullName (val first: String, val last:String)

object FullName {

  /**
    * Removing case class requires that we implement a singleton with apply() and
    * unapply() so that construction and deconstruction can work.
    */
  def apply(first: String, last: String): FullName = new FullName(first, last)

  def unapply(fullName: FullName): Option[(String, String)] = Some(fullName.first, fullName.last)
}
