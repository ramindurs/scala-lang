package akkactorsandstreams.model

import java.time.LocalDateTime

import io.circe.Json
import io.circe.parser._
import io.circe.syntax._
import org.scalatest.FlatSpec

class MessageSpec extends FlatSpec {

  private val dateTime = LocalDateTime.of(2019, 5, 30, 12, 11, 45)

  private lazy val jsonStr: String =
    s"""
       |{
       |  "id" : "1",
       |  "seconds" : 10,
       |  "dateTime" : "2019-05-30T12:11:45"
       |}
       """.stripMargin
  lazy val json: Json = parse(jsonStr).getOrElse(Json.Null)

  lazy val expectedMessageCreate = MessageCreate(1.toString, 10, dateTime)

  lazy val expectedMessageUpdate = MessageUpdate(1.toString, 10, dateTime)

  "MessageCreate encoder" should "generate JSON" in {
    val result: Json = expectedMessageCreate.asJson

    assert(json == result)
  }

  "MessageCreate decoder" should "generate case class" in {
    val actualMessage = json.as[MessageCreate]

    assert(Right(expectedMessageCreate) == actualMessage)
  }

  "MessageUpdate encoder" should "generate JSON" in {
    val result: Json = expectedMessageUpdate.asJson

    assert(json == result)
  }

  "MessageUpdate decoder" should "generate case class" in {
    val actualMessage = json.as[MessageUpdate]

    assert(Right(expectedMessageUpdate) == actualMessage)
  }

}
