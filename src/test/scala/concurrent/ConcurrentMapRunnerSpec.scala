package concurrent

import org.scalatest.FlatSpec

class ConcurrentMapRunnerSpec extends FlatSpec {

  val sut = new ConcurrentMapRunner

  "Adding keys" should "return a set of keys" in {
    sut.put("1", "a")
    sut.put("2", "b")
    sut.put("3", "c")

    val keys = sut.getKeys()

    assert(List() == keys)
  }
}
