package games.cointoss

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, InputStream}

import org.scalatest.{FlatSpec, Matchers}

class InteractiveReaderSpec extends FlatSpec with Matchers {

  "It" should "write to outputstream and read from inputstream" in {
    val input = "H"
    val inputStream: InputStream = new ByteArrayInputStream(input.getBytes)
    val outputStream = new ByteArrayOutputStream

    val msg = "Your Choice?"


    val result = InteractiveReader.readString(inputStream, outputStream, msg)

    result shouldBe input
    outputStream.toString shouldBe msg
  }

}
