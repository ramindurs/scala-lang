package challenges.springer.command

import org.scalatest.{FlatSpec, Matchers}

class CommandSpec extends FlatSpec with Matchers{

  "It" should "interpret Canvas command" in {
    val canvas = Command.from("C 20 4")

    canvas shouldBe CreateCanvas(20, 4)
  }

  "It" should "interpret Line command" in {
    val line = Command.from("L 1 2 6 2")

    line shouldBe Line(1,2,6,2)
  }

  "It" should "interpret Rectangle command" in {
    val box = Command.from("R 16 1 20 3")

    box shouldBe Rectangle(16, 1, 20, 3)
  }

  "It" should "interpret BucketFill command" in {
    val bf = Command.from("B 10 3 o")

    bf shouldBe BucketFill(10, 3, "o".charAt(0))
  }

  "It" should "interpret Quit command" in {
    val quit = Command.from("Q")

    quit shouldBe Quit()
  }

  "It" should "handle non-supported instructions" in {
    val error = Command.from("Q 12 34")

    error shouldBe Error("Instruction [Q 12 34] not supported")
  }
}
