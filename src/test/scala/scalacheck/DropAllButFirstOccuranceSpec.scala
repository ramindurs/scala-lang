package scalacheck

import org.scalacheck.Properties
import org.scalacheck.Prop.forAll

object DropAllButFirstOccuranceSpec extends Properties("DropAllButFirsOccuranceSpec") {

  val testIntList = List(1, 2, 3, 2, 4, 2, 5, 9, 10)

  val testStringList = List("test", "hello", "test", "x", "y", "x", "z")

  property("dropAllButFirstOccurance for int") = forAll { input: Int =>
    val result = Listutils.dropAllButFirstOccurance(testIntList, input)
    result.count(p => p == input) <= 1
  }
}
