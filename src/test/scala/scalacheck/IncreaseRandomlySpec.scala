package scalacheck

import org.scalacheck.Properties
import org.scalacheck.Prop.forAll

object IncreaseRandomlySpec extends Properties("IncreaseRandomlySpec"){

  property("increaseRandomly") = forAll { input: Int =>
    println(s"Testing $input")
    val result = MathUtils.increaseRandomly(input)
    result > input
  }
}
